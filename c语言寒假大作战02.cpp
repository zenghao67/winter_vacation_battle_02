#include<stdio.h>
#include <stdlib.h>
void menu(); 
void help();
void A(); 
void B();
void C(); 
void error();
int main()
{
    int n=1,x;
    printf("==========口算生成表==========\n");
    printf("欢迎使用口算生成器\n");
    printf("\n");
    help(); 
    printf("\n"); 
    while(n!=0)
    {
        menu();
        printf("请输入操作>");
        scanf("%d",&x);
        printf("<执行操作:)\n");
        printf("\n");
        printf("\n");
        switch(x)
        {
            case 1:A();break;
            case 2:B();break;
            case 3:C();break;
            case 4:help();break;
            case 5:
                n=0;
                printf("程序结束, 欢迎下次使用\n");
				printf("请按任意键继续...\n");
                break;
            default:error();break;  
        }
    }
    return 0;
}

void menu()
{
    printf("操作列表:\n");
    printf("1)一年级    2)二年级    3)三年级\n");
    printf("4)帮助      5)退出程序\n");
}
void help()
{
    printf("帮助信息\n");
    printf("您需要输入命令代号来进行操作, 且\n");
    printf("一年级题目为不超过十位的加减法;\n");
    printf("二年级题目为不超过百位的乘除法;\n");
    printf("三年级题目为不超过百位的加减乘除混合题目.\n");
}
void A()
{
    printf("现在是一年级题目:\n");
    printf("执行完了,(假装这里有操作\n");
    printf("\n");
    printf("\n");
}
void B()
{
    printf("现在是二年级题:\n");
    printf("执行完了,(假装这里有操作\n");
    printf("\n");
    printf("\n");
}
void C()
{
    printf("现在是三年级题目:\n");
    printf("执行完了,(假装这里有操作\n");
    printf("\n");
    printf("\n");
}
void error()
{
    printf("Error!!!\n");
    printf("错误操作指令, 请重新输入\n");
    printf("\n");
    printf("\n");
} 

