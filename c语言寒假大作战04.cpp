#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<math.h>
#include<string.h>
void menu(); 
void help();
void A(); 
void B();
void C(); 
void error();
int main()
{
    int n=1,x;
    printf("==========口算生成表==========\n");
    printf("欢迎使用口算生成器\n");
    printf("\n");
    help();
    printf("\n");
    while(n!=0)
    {
        menu();
        printf("请输入操作>");
        scanf("%d",&x);
        printf("<执行操作:)\n");
        printf("\n");
        printf("\n");
        switch(x)
        {
            case 1:A();break;
            case 2:B();break;
            case 3:C();break;
            case 4:help();break;
            case 5:
                n=0;
                printf("程序结束, 欢迎下次使用\n");
				printf("请按任意键继续...\n");
                break;
            default:error();break;  
        }
    }
    return 0;
}

void menu()
{ 
    printf("操作列表:\n");
    printf("1)一年级    2)二年级    3)三年级\n");
    printf("4)帮助      5)退出程序\n");
}
void help()
{
    printf("帮助信息\n");
    printf("您需要输入命令代号来进行操作, 且\n");
    printf("一年级题目为不超过十位的加减法;\n");
    printf("二年级题目为不超过百位的乘除法;\n");
    printf("三年级题目为不超过百位的加减乘除混合题目.\n");
}
void A()
{
	int o,a,b,c,D;
	time_t t;
	srand((unsigned) time(&t));
    printf("现在是一年级题目:\n");
    printf("请输入生成个数>\n");
    scanf("%d",&o);
    printf("<执行操作:)\n");
    for (int i=0;i<o;i++)
	{
    	a=rand() % 10;
    	b=rand() % 10;
    	c=rand() % 2;
    	if (c==0)
    	{
		
    	   D=a+b;
    	   printf("%d + %d = %d\n",a,b,D);
		}
    	else
    	{
		   D=a-b;
    	   printf("%d - %d = %d\n",a,b,D);
		   }
	}
}
void B()
{
	int o,a,b,c;
	float D;
	time_t t;
	srand((unsigned) time(&t));
    printf("现在是二年级题目:\n");
    printf("请输入生成个数>\n");
    scanf("%d",&o);
    printf("<执行操作:)\n");
    for (int i=0;i<o;i++)
	{
    	a=rand() % 10;
    	b=rand() % 10;
    	c=rand() % 2;
    	if (c==0)
    	{
    	   D=a*b;
    	   printf("%d * %d = %g\n",a,b,D);
		   }
    	else{
		while(b==0)
		{
			b=rand() % 10;
		 } 
		   D=a/(b*1.0);
    	   printf("%d / %d = %g\n",a,b,D);
		   }
	}
}
void C()
{
	int o,d,i,a,b,c;
	float z;
	char fh1[2],fh2[2];
	time_t t;
	srand((unsigned) time(&t));
    printf("现在是三年级题目:\n");
    printf("请输入生成个数>\n");
    scanf("%d",&o);
    printf("<执行操作:)\n");
    char fh[4][6] = {"*","/","+","-"};
     for (int i=0; i<o; i++)
	{
    	a=rand() % 100;
    	b=rand() % 100;
    	c=rand() % 100;
    	while (a==0||b==0||c==0)
    	{
    		a=rand() % 100;b=rand() % 100;c=rand() % 100; 
		}
 		strcpy(fh1,fh[rand() % 3]);
 		strcpy(fh2,fh[rand() % 3]);
		if(strcmp(fh1,"*")==0&&strcmp(fh2,"*")==0)
		{
			z = a * b * c;
		printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
		}
		else if(strcmp(fh1,"*")==0&&strcmp(fh2,"/")==0)
		{
			z = a * b / (c*1.0);
			printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
		}
		else if(strcmp(fh1,"*")==0&&strcmp(fh2,"+")==0)
		{
			z = a * b + c;
			printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
		}
		else if(strcmp(fh1,"*")==0&&strcmp(fh2,"-")==0)
		{
			z = a * b - c;
			printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
		}
		else if(strcmp(fh1,"/")==0&&strcmp(fh2,"+")==0)
		{
			z = a / (b*1.0) + c;
			printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
		}
		else if(strcmp(fh1,"/")==0&&strcmp(fh2,"-")==0)
		{
			z = a / (b*1.0) - c;
			printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
		}
		else if(strcmp(fh1,"/")==0&&strcmp(fh2,"/")==0)
		{
			z = a / (b*1.0) / (c*1.0);
			printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
		}
		else if(strcmp(fh1,"+")==0&&strcmp(fh2,"-")==0)
		{
			z = a + b - c;
			printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
		}
		else if(strcmp(fh1,"+")==0&&strcmp(fh2,"+")==0)
		{
			z = a + b + c;
			printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
		}
		else if(strcmp(fh1,"-")==0&&strcmp(fh2,"-")==0)
		{
			z = a - b - c;
			printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
		}
		else if(strcmp(fh1,"/")==0&&strcmp(fh2,"*")==0)
		{
			z = a / (b*1.0) * c;
			printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
		}
		else if(strcmp(fh1,"+")==0&&strcmp(fh2,"/")==0)
		{
			z = a + b / (c*1.0);
			printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
		}
		else if(strcmp(fh1,"+")==0&&strcmp(fh2,"*")==0)
		{
			z = a + b * c;
			printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
		}
		else if(strcmp(fh1,"-")==0&&strcmp(fh2,"/")==0)
		{
			z = a - b / (c*1.0);
			printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
		}
		else if(strcmp(fh1,"-")==0&&strcmp(fh2,"+")==0)
		{
			z = a - b + c;
			printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
		}
		else if(strcmp(fh1,"-")==0&&strcmp(fh2,"*")==0)
		{
			z = a - b * c;
			printf("%2d %s %2d %s %2d = %g\n",a,fh1,b,fh2,c,z);
		}
    }
    
}
void error()
{
    printf("Error!!!\n");
    printf("错误操作指令, 请重新输入\n");
    printf("\n");
    printf("\n");
}
